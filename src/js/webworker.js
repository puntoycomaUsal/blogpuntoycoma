//Crearmos el worker
var myWorker	= new Worker("../js/webworker_thread.js");

//Si recibimos un mensaje será el total del resultado
myWorker.addEventListener('message', function(e) {
	console.log("Mensaje recibido del worker", e.data);
	document.getElementById("result").value = parseInt(e.data);
}, false);

//Función que se ejecutará para enviar un mensaje al worker
function sendMessageToWorker() {
	myWorker.postMessage({ num: parseInt(document.getElementById("value").value) });
}

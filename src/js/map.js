/*
 * Cuando se ha ejecutado en Android para probar que muestre el geolocalizador y la
 * pregunta, no ha funcionado cuando he intentado cargarlo a través, 
 * de la dirección ip del equipo de mi red wifi donde se encuentra
 * mi servidor apache, ha cargado los puntos que tenía por defecto, al
 * cargar a través del depurador de Chrome se obtenie el siguiente mensaje:
 * 
 * [Deprecation] getCurrentPosition() and watchPosition() no longer work on insecure 
 * origins. To use this feature, you should consider switching your application to a 
 * secure origin, such as HTTPS. See https://goo.gl/rStTGz for more details. initMap 
 * @ map.js:16
 * 
 * Según parece es necesario disponer de una implementación http+s (http+ssl) para
 * que se activen estas dos funciones. Pero según indica la información dispuesta
 * en el enlace, cuando el dominio es http://localhost, este se considera seguro,
 * he cargado la página a traveś de la dirección http://localhost:8080 y esta vez
 * ha preguntado y geolocaizado correctamente. 
 * 
 **/

function initMap() {
	//Se cargarán unas coordenadas por defecto, por si el usuario decide no 
	//otorgarnos permiso.
	var uluru 	= {lat: -25.363, lng: 131.044};
	var	zoom	= 18;
	
	var map = new google.maps.Map(document.getElementById('map'), {
		zoom		: zoom,
		center		: uluru
	});
	var marker = new google.maps.Marker({
		position	: uluru,
		map			: map
	});
	
	navigator.geolocation.getCurrentPosition(function(position) {
		
		//Mostramos por consola javascript los valores recuperados para depuración.
		console.log('getCurrentPosition --> '+position.coords.latitude+','+position.coords.longitude);
		
		//Usamos el mismo objeto de coordenadas
		if (typeof uluru !== 'undefined'){
			//Si uluru no está definida es porque el mapa no se cargó aún, recordar que son listeners, se
			//disparan por eventos.
			uluru.lat	= position.coords.latitude;
			uluru.lng 	= position.coords.longitude;
		
			//Si el usuario nos permitió acceder a su ubicación, iremos a ella.
			map.setCenter(uluru,zoom);
			marker.setPosition(uluru);
		}
	});
	
	// Se ejecutará cada vez que cambie la posición
	navigator.geolocation.watchPosition(function(position) {
		
		//Mostramos por consola javascript los valores recuperados para depuración.
		console.log('watchPosition --> '+position.coords.latitude+position.coords.longitude);
		
			//Usamos el mismo objeto de coordenadas
			if (typeof uluru !== 'undefined'){
				//Si uluru no está definida es porque el mapa no se cargó aún, recordar que son listeners, se
				//disparan por eventos.
				uluru.lat	= position.coords.latitude;
				uluru.lng 	= position.coords.longitude;
			
				//Si el usuario nos permitió acceder a su ubicación, iremos a ella.
				map.setCenter(uluru,zoom);
				marker.setPosition(uluru);
			}
	});

}

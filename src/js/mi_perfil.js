
document.getElementById("captureImage").onchange = function () {
    var reader = new FileReader();

    reader.onload = function (e) {
        document.getElementById("image").src = e.target.result;
    };

    reader.readAsDataURL(this.files[0]);
};

window.onload = function () {

	//Control de errores
	try {
		//Si el navegador soporta localStorage, procedemos a buscar y cargar sus datos
		var usuario = JSON.parse(window.localStorage.getItem("usuario"))
		if (usuario !== null) {
			//Cargando datos al formulario
			document.getElementById("firstName").value = usuario.firstName
			document.getElementById("lastName").value = usuario.lastName;
			document.getElementById("phone").value = usuario.phone;
			document.getElementById("email").value = usuario.email;
			document.getElementById("birthdate").value = usuario.birthdate;
			document.getElementById("height").value = usuario.height;
			document.getElementById("favoriteColor").value = usuario.favoriteColor;
			document.getElementById("favoriteWeb").value = usuario.favoriteWeb;
		}else {
			console.log("No hay datos.")
		}
	} catch(e) {
		if (e instanceof DOMException) {
			console.log("Se ha producido un error --> "+e.message+" (Compruebe si se navegador soporta localStorage)");
		} else {
			console.log("Se ha producido un error --> "+e.message);
		}
	}
}


document.getElementById("save").onclick = function () {
	try {
		var user = {}
		console.log("Guardando datos del usuario.")
		user["firstName"] = document.getElementById("firstName").value;
		user["lastName"] = document.getElementById("lastName").value;
		user["phone"] = document.getElementById("phone").value;
		user["email"] = document.getElementById("email").value;
		user["birthdate"] = document.getElementById("birthdate").value;
		user["height"] = document.getElementById("height").value;
		user["favoriteColor"] = document.getElementById("favoriteColor").value;
		user["favoriteWeb"] = document.getElementById("favoriteWeb").value;		
		// Guardando de datos del formulario en localStorage
		window.localStorage.setItem("usuario", JSON.stringify(user));
	} catch(e) {
		if (e instanceof DOMException) {
			console.log("Se ha producido un error --> "+e.message+" (Compruebe si se navegador soporta localStorage)");
		} else {
			console.log("Se ha producido un error --> "+e.message);
		}
	}
}

/** Api javascript validación de campos **/

/**
 * Se ha decidido validar únicamente si el campo está relleno
 * cuando este es requerido, se han marcado algunos campos como
 * requeridos, es decir, como obligatorios, para mostrar más
 * tipos de validaciones posibles. 
 **/

var form  				= document.getElementById('mi_perfil');
var firstName			= document.getElementById('firstName');
var error_firstName		= document.getElementById('error_firstName');
var lastName			= document.getElementById('lastName');
var error_lastName		= document.getElementById('error_lastName')
var phone				= document.getElementById('phone');
var error_phone			= document.getElementById('error_phone')
var email 				= document.getElementById('email');
var error_email 		= document.getElementById('error_email');

firstName.addEventListener("change", function (event) {
	//En el momento que cambie el contenido del campo, testearemos si es
	//válido
	if (firstName.validity.valid) {
		//Si ya es válido, quitamos el texto de error y ocultamos la capa. 
		error_firstName.innerHTML 	= ""; 
		error_firstName.className 	= "error"; 
	}
}, false);

lastName.addEventListener("change", function (event) {
	//En el momento que cambie el contenido del campo, testearemos si es
	//válido
	if (lastName.validity.valid) {
		//Si ya es válido, quitamos el texto de error y ocultamos la capa. 
		error_lastName.innerHTML 	= ""; 
		error_lastName.className 	= "error"; 
	}
}, false);

phone.addEventListener("change", function (event) {
	//En el momento que cambie el contenido del campo, testearemos si es
	//válido
	if (phone.validity.valid) {
		//Si ya es válido, quitamos el texto de error y ocultamos la capa. 
		error_phone.innerHTML 		= ""; 
		error_phone.className 		= "error"; 
	}
}, false);

email.addEventListener("change", function (event) {
	//En el momento que cambie el contenido del campo, testearemos si es
	//válido
	if (email.validity.valid) {
		//Si ya es válido, quitamos el texto de error y ocultamos la capa. 
		error_email.innerHTML 		= ""; 
		error_email.className 		= "error"; 
	}
}, false);

form.addEventListener("submit", function (event) {	
	//Solo vamos a validar el formulario cuando el usuario intente enviarlo
	//entonces aparecerán los distintos mensajes de error y en el caso de que
	//alguno no pase la validación, se cancela el envío. 
	
	if (!firstName.validity.valid) {
		error_firstName.innerHTML 		= "El nombre es requerido";
		error_firstName.className 		= "error active";
		// Cancelamos el envío
		event.preventDefault();
	}
	
	if (!lastName.validity.valid) {
		error_lastName.innerHTML 		= "El apellido es requerido";
		error_lastName.className 		= "error active";
		// Cancelamos el envío
		event.preventDefault();
	}
	
	if (!phone.validity.valid) {
		error_phone.innerHTML 			= "El teléfono es requerido";
		error_phone.className 			= "error active";
		// Cancelamos el envío
		event.preventDefault();
	}
	
	if (!email.validity.valid) {
		error_email.innerHTML 			= "La dirección de correo electrónico es requerida";
		error_email.className 			= "error active";
		// Cancelamos el envío
		event.preventDefault();
	}
}, false);

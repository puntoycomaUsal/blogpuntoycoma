// Código javascript

function updateStatus() {
	console.log("isOnline?: " + navigator.onLine)
	var x = document.getElementById('status');
    var s = document.getElementById('statusColor');

  	x.innerHTML = navigator.onLine ? 'Conectado' : 'Desconectado';  
  	s.className = navigator.onLine ? 'main-header__status-color--online' : 'main-header__status-color--offline';
}
window.addEventListener('online', updateStatus);
window.addEventListener('offline', updateStatus);
window.onload = function(){
	updateStatus();
};
